# What is this?

Greetings.

This repository contains the project of iOS application, which should be done to pass the first step of a long-long way to become a Senior iOS developer in "Revolut" company.

# Language

-   app is written in Swift. Seriously, guys, if you still code in Objective-C - you'll better transfer you code in Swift as soon as possible.
-   app is written with [raywenderlich.com Swift Style Guide](https://github.com/raywenderlich/swift-style-guide)

# Architecture

App is written via "Apple-MVC" pattern. It has separated Model & View, and ViewControllers do all the work. Model is mostly passive, except the NSManagedObjectContext which notify Controller about changes via NSFetchedResultsController.

# Documentation
Class reference is presented in `\docs` folder.

# Tests

- Unit test examples are presented in `testApplicationTests` target. It's fully a unit-test target and it doesn't even run application.
- UI test examples are pressented in `testApplicationUITests` target.