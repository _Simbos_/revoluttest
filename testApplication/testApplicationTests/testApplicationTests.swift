//
//  testApplicationTests.swift
//  testApplicationTests
//
//  Created by Andrew Simvolokov on 26/12/2016.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import XCTest
@testable import testApplication

/**
 Here are some example of tests. This TestCases is just a unit-test, It doesn't even run application.
 */
class testApplicationTests: XCTestCase {
    /**
     Test of CurrencyRatePackParser.
     */
    func testCurrencyRatePackParser(){
        let bundle = Bundle(for: self.classForCoder)
        guard let fileURL = bundle.url(forResource: "eurofxref-daily", withExtension: "xml") else {
            XCTFail("test XML-file wasn't found")
            return
        }
        guard let currencyParser = CurrencyRatePackParser(contentsOfURL: fileURL as NSURL) else {
            XCTFail("currencyParser wasn't created")
            return
        }

        let expectation = self.expectation(description: "Waiting for parser")

        currencyParser.parse{ (errorMessage, currencyRates, senderName, senderDate) in
            if errorMessage != nil {
                XCTFail("Parser returned an error")
            }
            else{
                XCTAssertEqual(senderName, "European Central Bank")
                if senderDate != nil {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd"
                    XCTAssertEqual(formatter.string(from:senderDate!), "2016-07-06")
                }
                else{
                    XCTFail("senderDate is nil")
                }

                for (currencyType, currencyRate) in currencyRates {
                    switch currencyType {
                    case .USD:
                        XCTAssert(currencyRate.compare(NSDecimalNumber(value: 1.1069)) == .orderedSame, "Parser: USD - Bad value")
                    case .EUR:
                        ()
                    case .GBP:
                        XCTAssert(currencyRate.compare(NSDecimalNumber(value: 0.853)) == .orderedSame, "Parser: GBP - Bad value")
                    }
                }
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5.0) { error in
            _ = error.map{ print("Parser: timeout \($0.localizedDescription)") }
        }

    }

    /**
     Test of CurrencyRatePackParser. Use of wrong file.
     */
    func testCurrencyRatePackParserAbort(){
        let bundle = Bundle(for: self.classForCoder)
        guard let fileURL = bundle.url(forResource: "eurofxref-daily-err", withExtension: "xml") else {
            XCTFail("test XML-file wasn't found")
            return
        }
        guard let currencyParser = CurrencyRatePackParser(contentsOfURL: fileURL as NSURL) else {
            XCTFail("currencyParser wasn't created")
            return
        }

        let expectation = self.expectation(description: "Waiting for parser")

        currencyParser.parse{ (errorMessage, currencyRates, senderName, senderDate) in
            if errorMessage == nil {
                XCTFail("Parser didn't returned an error")
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5.0) { error in
            _ = error.map{ print("Parser: timeout \($0.localizedDescription)") }
        }
    }
}
