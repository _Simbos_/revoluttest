//
//  BackgroundController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreMotion

/**
 This UIViewController control background dynamic view of application.
 */
class BackgroundController: UIViewController {
    @IBOutlet private var overlayView: UIView!
    @IBOutlet private var centerY: NSLayoutConstraint!
    @IBOutlet private var centerX: NSLayoutConstraint!

    private var motionManager = CMMotionManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        motionManager.accelerometerUpdateInterval = 0.1

        if motionManager.isAccelerometerAvailable {
            let queue = OperationQueue()
            motionManager.startAccelerometerUpdates(to: queue, withHandler: { (accelerometerData, error) -> Void in
                DispatchQueue.main.async {
                    self.accelerometerHandler(accelerometerData, error: error)
                }
            })
        }
    }

    private var lastAngles: [Double] = [0,0]
    /**
     Updates background view via accelerometer data. This method is called by motion manager.
     */
    func accelerometerHandler(_ accelerometerData: CMAccelerometerData?, error: Error?){
        let rawData = [accelerometerData!.acceleration.y, accelerometerData!.acceleration.x]
        let anglesData = rawData.map {
            ((($0 + 2.0)) / 4.0 - 0.5) * 360
        }
        //treshhold
        if (abs(lastAngles[0]-anglesData[0]) > 5) ||
            (abs(lastAngles[1]-anglesData[1]) > 5){
            lastAngles = anglesData

            let angles = anglesData.map{ CGFloat(Int($0))/2 }
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.1, animations: {
                    self.centerY.constant = -angles[0]
                    self.centerX.constant = -angles[1]
                    self.view.layoutIfNeeded()
                })

            }
        }


    }
}
