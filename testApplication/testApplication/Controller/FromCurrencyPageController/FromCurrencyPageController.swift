//
//  FromCurrencyPageController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//
import UIKit

/**
 This UIPageViewController controls source("sell") part of interface.
 */
class FromCurrencyPageController: UIPageViewController {
    /**
     Updates subViewControllers with new **UserData** object.
     - parameter userData: new **UserData** object.
     */
    func updateControllers(userData: UserData){
        if let fromCurrencyViewControllers = self.viewControllers as? [FromCurrencyController]{
            for fromCurrencyViewController in fromCurrencyViewControllers {
                fromCurrencyViewController.storedMoney = userData.moneyForCurrency(fromCurrencyViewController.currencyType)
            }
        }
    }

    /**
     Updates subViewControllers with new currency rates. Recounts keeped values(amount of money for sell operation).
     - parameter currentToValue: current destination amount of money.
     - parameter currentToCurrencyType: current destination currency type.
     - parameter currencyRatePack: new packet of currency rates.
     */
    func updateControllers(currentToValue: NSDecimalNumber?, currentToCurrencyType: CurrencyType, currencyRatePack: CurrencyRatePack?){
        if let fromCurrencyViewControllers = self.viewControllers as? [FromCurrencyController]{
            for fromCurrencyViewController in fromCurrencyViewControllers {
                if (currentToValue != nil) && (currencyRatePack != nil){
                    if let rate = CurrencyRatePack.rateOfPack(currencyRatePack!, inContext: currencyRatePack!.managedObjectContext!, from: currentToCurrencyType, to: fromCurrencyViewController.currencyType){
                        fromCurrencyViewController.value = currentToValue!.multiplying(by: rate, withBehavior: NSDecimalNumberHandler(roundingMode: .down, scale: 0, raiseOnExactness: true, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true))
                    } else {
                        fromCurrencyViewController.value = nil
                    }
                } else {
                    fromCurrencyViewController.value = nil
                }
            }
        }
    }
}

