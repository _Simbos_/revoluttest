//
//  FromCurrencyController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

/**
 This UIViewController outputs sell information about some currency.
 */
class FromCurrencyController: CurrencyController {
    override class var storyboardID: String {
        return "FromCurrencyController"
    }

    override var signedSymbol: String {
        return "-"
    }

    override func reloadStoredMoneyLabel(){
        super.reloadStoredMoneyLabel()

        if (value == nil) || (value?.compare(storedMoney) != .orderedDescending) {
            self.storedMoneyLabel.textColor = UIColor.white
        } else {
            self.storedMoneyLabel.textColor = UIColor.red
        }
    }

    override func reloadValueTextField(){
        super.reloadValueTextField()
        self.reloadStoredMoneyLabel()
    }
}
