//
//  CurrencyController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData

/**
 The delegate of **CurrencyController** must adopt the **CurrencyControllerDelegate**.
 */
protocol CurrencyControllerDelegate: class {
    /// Sent when user has changed value keeped by CurrencyController.
    func currencyController(_ currencyController: CurrencyController, valueWasChanged value: NSDecimalNumber?)
}

/**
 Abstract CurrencyController which outputs infromation about some currency.
 */
class CurrencyController: UIViewController {
    /**
     Static class variant. Ony one controller could be a responder in one moment.
     */
    static var firstResponder: CurrencyController?

    /// This textfield outputs/input amount of money for buy/sell.
    @IBOutlet var valueTextField: UITextField!
    /// This label outputs information about keeped currency type.
    @IBOutlet var currencyTypeLabel: UILabel!
    /// This label outputs information about keeped amout of money.
    @IBOutlet var storedMoneyLabel: UILabel!

    /// This formatter is used to output value in textfield
    let outputFormatter: NumberFormatter
    /// This formatter is used to input value in textfield
    let inputFormatter: NumberFormatter

    required init?(coder aDecoder: NSCoder) {
        //formatters inizialization
        outputFormatter = NumberFormatter()
        outputFormatter.numberStyle           = .decimal
        outputFormatter.multiplier            = NSNumber(value: 0.01)
        outputFormatter.maximumFractionDigits = 2
        outputFormatter.generatesDecimalNumbers = true
        outputFormatter.usesGroupingSeparator = true
        outputFormatter.negativePrefix = "z"//impossible prefix
        outputFormatter.decimalSeparator = "."
        outputFormatter.groupingSeparator = ","

        inputFormatter = NumberFormatter()
        inputFormatter.numberStyle           = .decimal
        inputFormatter.multiplier            = NSNumber(value: 0.01)
        inputFormatter.maximumFractionDigits = 2
        inputFormatter.generatesDecimalNumbers = true
        inputFormatter.usesGroupingSeparator = false
        inputFormatter.negativePrefix = "z"//impossible prefix
        inputFormatter.decimalSeparator = NSLocale.current.decimalSeparator

        super.init(coder: aDecoder)
        outputFormatter.positivePrefix = self.signedSymbol
        inputFormatter.positivePrefix = self.signedSymbol
    }

    /// delegate must adopt the **CurrencyControllerDelegate**
    weak var delegate: CurrencyControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        valueTextField.delegate = self
        valueTextField.addTarget(self, action: #selector(CurrencyController.textFieldValueWasChanged(_:)), for: .editingChanged)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if CurrencyController.firstResponder == nil {
            CurrencyController.firstResponder = self
            self.valueTextField.becomeFirstResponder()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        //Making backcground transparent. It's easy to control UIViewController in XIB when it's background is not transparent. But in run mode it mist be transparent.
        self.view.backgroundColor = UIColor.clear
    }

    /// Type of currency which is diplayed in view.
    var currencyType: CurrencyType = .USD {
        didSet{
            reloadCurrencyTypeLabel()
            reloadStoredMoneyLabel()
        }
    }

    /// Amount of user's money which is diplayed in view.
    var storedMoney: NSDecimalNumber = 10000 {
        didSet{
            reloadStoredMoneyLabel()
        }
    }

    private var _value: NSDecimalNumber? = nil
    private var valueIsEditing = false
    /// Amount of money for sell/buy operation which is displayed in view.
    var value: NSDecimalNumber?  {
        set{
            _value = newValue
            reloadValueTextField()
        }
        get{
            return _value
        }
    }

    /// This simpole is a prefix for output value.
    var signedSymbol: String {
        fatalError("CurrencyController is an abstract class which doesn't have signedSymbol")
    }

    /// Pushes valueTextField to become a first responder. This method is used by external control.
    func valueBecomeFirstResponder() -> Bool {
        return self.valueTextField.becomeFirstResponder()
    }

    /// Reloads value of currency type.
    func reloadCurrencyTypeLabel(){
        DispatchQueue.main.async {
            self.currencyTypeLabel.text = self.currencyType.abbreviation
        }
    }

    /// Reloads value of stored user's monet.
    func reloadStoredMoneyLabel(){
        let formatter = self.currencyType.formatter
        let infoString = "You've got " + (formatter.string(from: storedMoney) ?? "")
        DispatchQueue.main.async {
            self.storedMoneyLabel.text = infoString
        }
    }

    /// Reloads amount of money for sell/buy operation.
    func reloadValueTextField(){
        if !valueIsEditing {
            DispatchQueue.main.async {
                guard let value = self.value, let numberString = self.outputFormatter.string(from: value) else {
                    self.valueTextField.text = nil
                    return
                }
                self.valueTextField.text = numberString
            }
        }
    }

    /// storyboard identifier.
    class var storyboardID: String {
        fatalError("CurrencyController.storyboardID.getter - abstract class doesn't have storyboardID")
    }

    /**
     This method is called by valueTextField every time when it's value is changed by user.
     */
    func textFieldValueWasChanged(_ textField: UITextField){
        valueIsEditing = true
        if let valueString = textField.text, valueString != self.signedSymbol {
            if valueString.characters.first != self.signedSymbol.characters.first {
                textField.text = self.signedSymbol + valueString
            }
            value = inputFormatter.number(from: textField.text!) as? NSDecimalNumber
        } else {
            textField.text = ""
            value = nil
        }
        valueIsEditing = false
        delegate?.currencyController(self, valueWasChanged: self.value)
    }
}
