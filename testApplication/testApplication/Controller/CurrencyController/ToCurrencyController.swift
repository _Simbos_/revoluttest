//
//  ToCurrencyController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

/**
 This UIViewController outputs buy information about some currency.
 */
class ToCurrencyController: CurrencyController {

    @IBOutlet private var exchangeRateLabel: UILabel!

    override var signedSymbol: String {
        return "+"
    }

    /// Displayed destination currency
    var toCurrencyType: CurrencyType {
        get {
            return self.currencyType
        }
        set{
            self.currencyType = newValue
            reloadExchangeRateLabel()
        }
    }

    /// Displayed source currency
    var fromCurrencyType: CurrencyType = .USD {
        didSet{
            reloadExchangeRateLabel()
        }
    }

    /// Displayed exchanged rate for source & destination currencies.
    var exchangeRate: NSDecimalNumber? {
        didSet{
            reloadExchangeRateLabel()
        }
    }

    private func reloadExchangeRateLabel(){
        DispatchQueue.main.async {
            if let exchangeRate = self.exchangeRate {
                let fromCurrencyString = self.fromCurrencyType.formatter.string(from: 100) ?? "Error"
                let toCurrencyString = self.toCurrencyType.formatter.string(from: exchangeRate.multiplying(by: 100)) ?? "Error"

                self.exchangeRateLabel.text = fromCurrencyString + "=" + toCurrencyString + "\n"
                self.exchangeRateLabel.textColor = UIColor.white
            } else {
                self.exchangeRateLabel.text = "exchange unavailable"
                self.exchangeRateLabel.textColor = UIColor.red
            }
        }
    }

    override class var storyboardID: String{
        return "ToCurrencyController"
    }
}
