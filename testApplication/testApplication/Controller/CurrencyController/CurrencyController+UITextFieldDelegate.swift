//
//  CurrencyController+UITextFieldDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

extension CurrencyController: UITextFieldDelegate {
    /// UITextFieldDelegate.textField(:shouldChangeCharactersInRange:replacementString:) handler.
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString = textField.text ?? ""
        let resultString = (currentString as NSString).replacingCharacters(in: range, with: string)

        if string == inputFormatter.decimalSeparator {
            if currentString.characters.count == 0 {
                return false
            }
            if currentString.range(of: string) != nil {
                return false
            }
        } else {
            let numberStrings = resultString.components(separatedBy: inputFormatter.decimalSeparator)
            if numberStrings.count > 1 {
                if numberStrings[1].characters.count > 2 {
                    return false
                }
            }
        }
        return true
    }

    /// UITextFieldDelegate.textFieldDidBeginEditing(:) handler.
    func textFieldDidBeginEditing(_ textField: UITextField) {
        CurrencyController.firstResponder = self
        if let value = self.value {
            textField.text = inputFormatter.string(from: value)
        }
    }

    /// UITextFieldDelegate.textFieldDidEndEditing(:) handler.
    func textFieldDidEndEditing(_ textField: UITextField) {
        reloadValueTextField()
    }
}
