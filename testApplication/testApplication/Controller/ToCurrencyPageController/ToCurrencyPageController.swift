//
//  ToCurrencyPageController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit
/**
 This UIPageViewController controls destination("buy") part of interface.
 */
class ToCurrencyPageController: UIPageViewController {
    /**
     Updates subViewControllers with new **UserData** object.
     - parameter userData: new **UserData** object.
     */
    func updateControllers(userData: UserData){
        if let toCurrencyViewControllers = self.viewControllers as? [ToCurrencyController]{
            for toCurrencyViewController in toCurrencyViewControllers {
                toCurrencyViewController.storedMoney = userData.moneyForCurrency(toCurrencyViewController.currencyType)
            }
        }
    }

    /**
     Updates subViewControllers with new currency rates. Recounts keeped values(amount of money for buy operation).
     - parameter currentFromValue: current source amount of money.
     - parameter currentFromCurrencyType: current source currency type.
     - parameter currencyRatePack: new packet of currency rates.
     */
    func updateControllers(currentFromValue: NSDecimalNumber?, currentFromCurrencyType: CurrencyType, currencyRatePack: CurrencyRatePack?){
        if let toControllerViewControllers = self.viewControllers as? [ToCurrencyController]{
            for toCurrencyViewController in toControllerViewControllers {
                if currencyRatePack != nil {
                    toCurrencyViewController.exchangeRate = CurrencyRatePack.rateOfPack(currencyRatePack!, inContext: currencyRatePack!.managedObjectContext!, from: currentFromCurrencyType, to: toCurrencyViewController.toCurrencyType)
                } else {
                    toCurrencyViewController.exchangeRate = nil
                }

                if (currentFromValue != nil) && (currencyRatePack != nil){
                    if let rate = CurrencyRatePack.rateOfPack(currencyRatePack!, inContext: currencyRatePack!.managedObjectContext!, from: currentFromCurrencyType, to: toCurrencyViewController.toCurrencyType){
                        toCurrencyViewController.value = currentFromValue?.multiplying(by: rate, withBehavior: NSDecimalNumberHandler(roundingMode: .down, scale: 0, raiseOnExactness: true, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true))
                    } else {
                        toCurrencyViewController.value = nil
                    }
                } else {
                    toCurrencyViewController.value = nil
                }
            }
        }
    }
}
