//
//  ExchangeController+NSFetchedResultsControllerDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

extension ExchangeController: NSFetchedResultsControllerDelegate {
    /// NSFetchedResultsControllerDelegate.controllerDidChangeContent(:) handler.
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller === userDataFRC {
            if let userData = controller.object(at: IndexPath(row: 0, section: 0)) as? UserData {
                fromCurrencyPageViewController.updateControllers(userData: userData)
                toCurrencyPageViewController.updateControllers(userData: userData)
            }
        } else if controller === currencyRatesFRC {
            if let currencyRatePack = controller.object(at: IndexPath(row: 0, section: 0)) as? CurrencyRatePack {
                exchangeModelController.currentRatePack = currencyRatePack

                toCurrencyPageViewController.updateControllers(currentFromValue: exchangeModelController.currentFromValue, currentFromCurrencyType: exchangeModelController.currentFromCurrency, currencyRatePack: exchangeModelController.currentRatePack)
            }
        }
        reloadExchangeButton()
    }
}
