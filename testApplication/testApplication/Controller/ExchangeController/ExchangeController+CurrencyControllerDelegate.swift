//
//  ExchangeController+CurrencyControllerDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

extension ExchangeController: CurrencyControllerDelegate {
    /// UICurrencyViewControllerDelegate.currencyViewController(:valueWasChanged:) handler.
    func currencyController(_ currencyController: CurrencyController, valueWasChanged value: NSDecimalNumber?) {
        switch currencyController {
        case is FromCurrencyController:
            exchangeModelController.currentFromValue = currencyController.value
            toCurrencyPageViewController.updateControllers(currentFromValue: exchangeModelController.currentFromValue, currentFromCurrencyType: exchangeModelController.currentFromCurrency, currencyRatePack: exchangeModelController.currentRatePack)
        case is ToCurrencyController:
            exchangeModelController.currentToValue = currencyController.value
            fromCurrencyPageViewController.updateControllers(currentToValue: exchangeModelController.currentToValue, currentToCurrencyType: exchangeModelController.currentToCurrency, currencyRatePack: exchangeModelController.currentRatePack)
        default:
            ()
        }
        reloadExchangeButton()
    }
}
