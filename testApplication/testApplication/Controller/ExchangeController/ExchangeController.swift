//
//  ExchangeController.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import CoreData
import UIKit

private let currencyTypes: [CurrencyType] = [.USD,.EUR,.GBP]

/**
 This is a big fat controller which observe users action with subviewcontrollers, updates them if needed and controls exhange-operations.
 */
class ExchangeController: UIViewController {

    @IBOutlet private var bottomOffsetConstaint: NSLayoutConstraint!
    /// invisible textfield is used for UIPageViewControllers switch operations. When user starts to change page, invisibleTextField become a first responder(if it's needed). So the KeyBoard doesn't dissapear.
    @IBOutlet private(set) var invisibleTextField: UITextField!
    @IBOutlet private var exchangeButton: UIBarButtonItem!

    /// This UIPageViewController controls "sell" part of view.
    private(set) var fromCurrencyPageViewController: FromCurrencyPageController!
    /// This UIPageViewController controls "buy" part of view.
    private(set) var toCurrencyPageViewController: ToCurrencyPageController!
    /// Model controller
    private(set) var exchangeModelController = ExchangeModelManager(pageCurrencyTypes: currencyTypes, context: CoreDataManager.sharedInstance.managedObjectContext)

    /// Observed managed object context.
    let managedObjectContext = CoreDataManager.sharedInstance.managedObjectContext

    private var _userDataFRC: NSFetchedResultsController<UserData>?
    /// This fecthed results controller observes changes of **UserData** in managed object context
    var userDataFRC: NSFetchedResultsController<UserData> {
        if _userDataFRC == nil {
            let fetchRequest: NSFetchRequest<UserData> = UserData.fetchRequest()
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "uuid", ascending: true)]

            let newFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            newFetchedResultsController.delegate = self
            _userDataFRC = newFetchedResultsController
        }
        return _userDataFRC!
    }

    private var _currencyRatesFRC: NSFetchedResultsController<CurrencyRatePack>?
    /// This fecthed results controller observes changes of **CurrencyRatePack** objects in managed object context.
    var currencyRatesFRC: NSFetchedResultsController<CurrencyRatePack> {
        if _currencyRatesFRC == nil {
            let fetchRequest: NSFetchRequest<CurrencyRatePack> = CurrencyRatePack.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "rates.@count >= 2")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "clientDate", ascending: false)]

            let newFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            newFetchedResultsController.delegate = self
            _currencyRatesFRC = newFetchedResultsController
        }
        return _currencyRatesFRC!
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //Setting navigation bar fully translucent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true

        do{
            try userDataFRC.performFetch()
        } catch {
            fatalError("userDataFRC.performFetch throwed an error: \(error)")
        }

        do{
            try currencyRatesFRC.performFetch()
        } catch {
            fatalError("currencyRatesFRC.performFetch throwed an error: \(error)")
        }

        self.exchangeButton.target = self
        self.exchangeButton.action = #selector(ExchangeController.exchangeButtonTapped)

        NotificationCenter.default.addObserver(self, selector: #selector(ExchangeController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        reloadExchangeButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let fromCurrencyViewController = self.fromCurrencyPageViewController?.viewControllers?.first as? FromCurrencyController {
            fromCurrencyViewController.valueBecomeFirstResponder()
        }
    }

    /**
     This method is called by notification when keyboard appears. It is used to control bottom offset of view.
     */
    func keyboardWillShow(_ notification: NSNotification) {
        if let info = notification.userInfo {
            let keyboardFrameLocal: CGRect     = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            let keyboardHeightLocal: CGFloat   = keyboardFrameLocal.height

            bottomOffsetConstaint.constant = keyboardHeightLocal
            self.view.layoutIfNeeded()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueID = segue.identifier else {
            return
        }

        switch segueID {
        case "fromCurrency":
            guard let fromCurrencyPageViewController = segue.destination as? FromCurrencyPageController else {
                fatalError("fromCurrencyPageViewController is not a UIPageViewController")
            }
            exchangeModelController.currencyViewControllerDelegate = self
            exchangeModelController.currentFromCurrency = exchangeModelController.pageCurrencyTypes[0]
            let fromCurrencyViewController = exchangeModelController.viewControllerAtIndex(0, identifier: FromCurrencyController.storyboardID, storyboard: storyboard!) as! FromCurrencyController
            fromCurrencyPageViewController.setViewControllers([fromCurrencyViewController], direction: .forward, animated: false, completion: {done in })
            fromCurrencyPageViewController.dataSource = exchangeModelController
            fromCurrencyPageViewController.delegate   = self
            self.fromCurrencyPageViewController = fromCurrencyPageViewController
        case "toCurrency":
            guard let toCurrencyPageViewController = segue.destination as? ToCurrencyPageController else {
                fatalError("toCurrencyPageViewController is not a UIPageViewController")
            }
            exchangeModelController.currencyViewControllerDelegate = self
            exchangeModelController.currentToCurrency = exchangeModelController.pageCurrencyTypes[1]
            let toCurrencyViewController = exchangeModelController.viewControllerAtIndex(1, identifier: ToCurrencyController.storyboardID, storyboard: storyboard!) as! ToCurrencyController
            toCurrencyPageViewController.setViewControllers([toCurrencyViewController], direction: .forward, animated: false, completion: {done in })
            toCurrencyPageViewController.dataSource = exchangeModelController
            toCurrencyPageViewController.delegate   = self
            self.toCurrencyPageViewController = toCurrencyPageViewController
        default:
            ()
        }
    }

    /**
     This method is called by button "Exchange" on tap.
     */
    func exchangeButtonTapped(){
        let fromCurrency = exchangeModelController.currentFromCurrency
        let toCurrency = exchangeModelController.currentToCurrency
        if let fromValue = exchangeModelController.currentFromValue, let toValue = exchangeModelController.currentToValue {
            NetworkManager.postExchangeOperation(fromCurrency: fromCurrency, fromValue: fromValue, toCurrency: toCurrency, toValue: toValue) { (result) in
                if result.isSuccess {
                    let userData = UserData.userData(inContext: self.managedObjectContext)
                    userData.exchange(fromCurrency: fromCurrency, fromValue: fromValue, toCurrency: toCurrency, toValue: toValue)

                } else {
                    let alertController = UIAlertController(title: "Error", message: "Server has rejected the operation", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }

    /**
     Reloads **exchangeButton**. It's enabled when user has enouth money for exchange operation & when source & destionation currencies are not equal.
     */
    func reloadExchangeButton(){
        if exchangeModelController.currentFromCurrency != exchangeModelController.currentToCurrency {
            if let fromValue = exchangeModelController.currentFromValue {
                let currentValue = UserData.userData(inContext: managedObjectContext).moneyForCurrency(exchangeModelController.currentFromCurrency)
                if fromValue.compare(currentValue) != .orderedDescending {
                    DispatchQueue.main.async {
                        self.exchangeButton.isEnabled = true
                    }
                    return
                }
            }
        }
        DispatchQueue.main.async {
            self.exchangeButton.isEnabled = false
        }
    }
}
