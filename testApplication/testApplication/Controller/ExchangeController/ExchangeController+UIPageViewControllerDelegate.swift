//
//  ExchangeController+UIPageViewControllerDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

extension ExchangeController: UIPageViewControllerDelegate {
    /// UIPageViewControllerDelegate.pageViewController(:willTransitionToViewControllers:) handler.
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
    //func pageViewController(_ pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [UIViewController]){
        if let currentViewController = pageViewController.viewControllers?.first as? CurrencyController {
            if currentViewController === CurrencyController.firstResponder {
                self.invisibleTextField.becomeFirstResponder()
            }
        }
        if let pendingViewController = pendingViewControllers.first as? CurrencyController {
            pendingViewController.storedMoney = UserData.userData(inContext: managedObjectContext).moneyForCurrency(pendingViewController.currencyType)
            switch pendingViewController {
            case is FromCurrencyController:
                if let rate = exchangeModelController.currentRateFrom(exchangeModelController.currentToCurrency, to: pendingViewController.currencyType), let currentToValue = exchangeModelController.currentToValue {
                    pendingViewController.value = currentToValue.multiplying(by: rate, withBehavior: exchangeModelController.roundUpHandler)
                } else {
                    pendingViewController.value = nil
                }
            case is ToCurrencyController:
                let rate = exchangeModelController.currentRateFrom(exchangeModelController.currentFromCurrency, to: pendingViewController.currencyType)
                if (rate != nil), let currentFromValue = exchangeModelController.currentFromValue{
                    pendingViewController.value = currentFromValue.multiplying(by: rate!, withBehavior: exchangeModelController.roundDownHandler)
                } else {
                    pendingViewController.value = nil
                }
                (pendingViewController as! ToCurrencyController).exchangeRate = rate
            default:
                ()
            }
        }
    }

    /// UIPageViewControllerDelegate.pageViewController(:didFinishAnimating:didFinishAnimating:transitionCompleted:) handler.
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let newCurrencyViewController = pageViewController.viewControllers?.first as? CurrencyController else {
            debugLog("Warning! pageViewController didFinishAnimating - newCurrencyViewController wasn't found!")
            return
        }

        if let previousViewController = previousViewControllers.first as? CurrencyController {
            if previousViewController === CurrencyController.firstResponder {
                DispatchQueue.main.async {
                    CurrencyController.firstResponder = newCurrencyViewController
                    newCurrencyViewController.valueBecomeFirstResponder()
                }
            }
        }
        if completed {
            switch pageViewController {
            case is FromCurrencyPageController:
                //update currency value
                exchangeModelController.currentFromCurrency = newCurrencyViewController.currencyType
                exchangeModelController.currentFromValue    = newCurrencyViewController.value
                //update toCurrencyViewController
                toCurrencyPageViewController.updateControllers(currentFromValue: exchangeModelController.currentFromValue, currentFromCurrencyType: exchangeModelController.currentFromCurrency, currencyRatePack: exchangeModelController.currentRatePack)
            case is ToCurrencyPageController:
                //update currency value
                exchangeModelController.currentToCurrency = newCurrencyViewController.currencyType
                exchangeModelController.currentToValue    = newCurrencyViewController.value
            default:
                ()
            }

            reloadExchangeButton()
        }
    }
}
