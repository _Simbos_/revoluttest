//
//  AppDelegate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 26/12/2016.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    /// Application window
    var window: UIWindow?

    /// Application's global currency rate controller.
    var currencyRateManager: CurrencyRateManager!

    /// UIApplicationDelegate.application(:didFinishLaunchingWithOptions:) handler.
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) -> Bool {
        currencyRateManager = CurrencyRateManager(managedObjectContext: CoreDataManager.sharedInstance.managedObjectContext)
        currencyRateManager.startCurrencyRateUpdating()

        return true
    }
    
}

