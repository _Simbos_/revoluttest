//
//  CoreDataManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/// name of Core Data model
private let modelName = "CurrencyAppModel"
/// name of database file
private let databaseFilename = "Cash.sqlite"

/**
 This class manages all data about Core Data Stack.
 */
class CoreDataManager: NSObject {
    /**
     Singleton of **CoreDataManager**.
     */
    static let sharedInstance = CoreDataManager()
    private override init() {super.init()}

    private var _managedObjectModel: NSManagedObjectModel? = nil
    /**
     Available object model.
     */
    var managedObjectModel: NSManagedObjectModel {
        if _managedObjectModel == nil {
            guard let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd") else {
                fatalError("URL of managedObjectModel could not be found")
            }
            _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
            guard _managedObjectModel != nil else {
                fatalError("managedObjectModel could not be loaded from URL")
            }
        }
        return _managedObjectModel!
    }

    private var _persistentStoreCoordinator: NSPersistentStoreCoordinator? = nil
    /*
     Available persistent store coordinator.
     */
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {
        if _persistentStoreCoordinator == nil {
            _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)

            let applicationURLArray = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let applicationDocumentsURL = applicationURLArray.last!
            let databaseURL = applicationDocumentsURL.appendingPathComponent(databaseFilename)

            do {
                try _persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: databaseURL, options: nil)
            } catch {
                fatalError("Unresolved error during adding of persistentStore:\(error), \((error as NSError).userInfo)")
            }
            guard _persistentStoreCoordinator != nil else {
                fatalError("persistentStoreCoordinator could not be loaded")
            }
        }
        return _persistentStoreCoordinator!
    }

    private var _managedObjectContext: NSManagedObjectContext? = nil
    /*
     Root managed object context.
     */
    var managedObjectContext: NSManagedObjectContext {
        if _managedObjectContext == nil {
            _managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            _managedObjectContext!.persistentStoreCoordinator = persistentStoreCoordinator
        }
        return _managedObjectContext!
    }
}
