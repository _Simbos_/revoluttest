//
//  CurrencyRatePack+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

extension CurrencyRatePack {
    @nonobjc class func fetchRequest() -> NSFetchRequest<CurrencyRatePack> {
        return NSFetchRequest<CurrencyRatePack>(entityName: "CurrencyRatePack")
    }

    /// Date when packet was inserted to database.
    @NSManaged var clientDate: NSDate
    /// Date when packet was created by server.
    @NSManaged var serverDate: NSDate?
    /// Server's(Organization's) name
    @NSManaged var sender: String?
    /// Set of currency rates
    @NSManaged var rates: NSSet?
}
