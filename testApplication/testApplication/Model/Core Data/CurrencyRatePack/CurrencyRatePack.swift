//
//  CurrencyRatePack.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/**
 **CurrencyRatePack** is a subclass of **NSManagedObject**. **CurrencyRatePack** is a group/packet of **CurrencyRate** which were downloaded by one single packet.
 In addition to **rates**, packet is also signed with name of server(**sender**), server's signed date(**serverDate**) and date when it was downloaded(**clientDate**)
 */
class CurrencyRatePack: NSManagedObject {

    override func awakeFromInsert() {
        self.clientDate = NSDate()
    }

    /**
     Inserts new rate packet in managed object context.
     - parameter context: managed object context where you want to insert new rate packet.
     - returns: inserted packet object.
     */
    class func insertRatePack(inContext context: NSManagedObjectContext) -> CurrencyRatePack {
        let ratePack = CurrencyRatePack(entity: CurrencyRatePack.entity(), insertInto: context)

        return ratePack
    }

    /**
     Gets rate of currency from packet in a specified managed object context.
     - parameter currencyType: type of currency you are searhing for.
     - parameter pack: packet of rates to search in. Pack could be from any context.
     - parameter context: managed object context to search in.
     - returns: found rate's value or **nil** if it wasn't found.
     */
    class func rateForCurrencyType(_ currencyType: CurrencyType, pack: CurrencyRatePack, inContext context: NSManagedObjectContext) -> NSDecimalNumber? {
        if currencyType == .EUR {// there is no need to search for rate of EUR, also packet could not even have that rate.
            return NSDecimalNumber.one
        }

        let packInContext = context.object(with: pack.objectID) as! CurrencyRatePack

        let fetchRequest: NSFetchRequest<CurrencyRate> = CurrencyRate.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "(pack == %@) AND currencyType = %i", packInContext, currencyType.rawValue)

        do {
            let rates = try context.fetch(fetchRequest)
            return rates.first?.rate
        } catch {
            debugLog("Error! managedObjectContext.executeFetchRequest thowed an error: \(error)")
        }
        return nil
    }

    /**
     Gets a rate between two currencies(1 x **X** = (rate) x **Y** ) from packet in a specified managed object context.
     - parameter pack: currency packet which contains rates of bouth currecnies.
     - parameter context: managed object context to search in.
     - parameter fromCurrencyType: source currency.
     - parameter toCurrencyType: destination currency
     - returns: found rate's value or **nil** if it wasn't found.
     */
    class func rateOfPack(_ pack: CurrencyRatePack, inContext context: NSManagedObjectContext, from fromCurrencyType: CurrencyType, to toCurrencyType: CurrencyType) -> NSDecimalNumber? {
        let fromRate = self.rateForCurrencyType(fromCurrencyType, pack: pack, inContext: context)
        let toRate = self.rateForCurrencyType(toCurrencyType, pack: pack, inContext: context)

        guard (fromRate != nil) && (toRate != nil) else {
            return nil
        }
        return toRate!.dividing(by: fromRate!, withBehavior: NSDecimalNumberHandler(roundingMode: .down, scale: 6, raiseOnExactness: true, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true))
    }

}
