//
//  CurrencyRatePack+Relationships.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation

extension CurrencyRatePack {
    //MARK: - rates references
    /**
     Adds currency rate to **rates**
     */
    func addRate(_ rate: CurrencyRate) {
        let set = NSSet(object: rate)
        addRates(set)
    }

    /**
     Removes currency rate from **rates**
     */
    func removeRate(_ rate: CurrencyRate) {
        let set = NSSet(object: rate)
        removeRates(set)
    }

    /**
     Adds set of currency rates to **rates**
     */
    func addRates(_ rates: NSSet) {
        addObjectSet("rates", objectNSSet: rates)
    }

    /**
     Removes set of currency rates from **rates**
     */
    func removeRates(_ rates: NSSet) {
        removeObjectSet("rates", objectNSSet: rates)
    }
}
