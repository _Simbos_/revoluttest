//
//  NSManagedObject+Relationships.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import CoreData
/**
 This extension presents functions to control one-to-many relationships of NSManagedObject.
 */
extension NSManagedObject {
    /**
     Adds set of objects to relationship.
     - parameter setKey: name of relationship.
     - parameter objectNSSet: **NSSet** of objects to be added.
     */
    func addObjectSet(_ relationship: String, objectNSSet: NSSet) {
        addObjectSet(relationship, objects: objectNSSet as Set<NSObject>)
    }

    /**
     Removes set of objects from relationship.
     - parameter setKey: name of relationship.
     - parameter objectNSSet: **NSSet** of objects to be removed.
     */
    func removeObjectSet(_ relationship: String, objectNSSet: NSSet) {
        removeObjectSet(relationship, objects: objectNSSet as Set<NSObject>)
    }

    /**
     Adds set of objects to relationship.
     - parameter setKey: name of relationship.
     - parameter objects: set of objects to be added.
     */
    func addObjectSet(_ setKey: String, objects: Set<NSObject>) {
        willChangeValue(forKey: setKey, withSetMutation: .union, using: objects)
        (primitiveValue(forKey: setKey) as AnyObject?)?.union(objects)
        didChangeValue(forKey: setKey, withSetMutation: .union, using: objects)
    }

    /**
     Removes set of objects from relationship.
     - parameter setKey: name of relationship.
     - parameter objects: set of objects to be removed.
     */
    func removeObjectSet(_ setKey: String, objects: Set<NSObject>) {
        willChangeValue(forKey: setKey, withSetMutation: .minus, using: objects)
        (primitiveValue(forKey: setKey) as AnyObject?)?.minus(objects)
        didChangeValue(forKey: setKey, withSetMutation: .minus, using: objects)
    }
}
