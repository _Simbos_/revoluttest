//
//  ExchangeModelManager+UIPageViewControllerDataSource.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit

extension ExchangeModelManager: UIPageViewControllerDataSource {
    /// UIPageViewControllerDataSource.viewControllerBeforeViewController handler.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! CurrencyController)//TODO: indicate an error
        if (index == NSNotFound) {
            return nil
        }
        index = (pageCurrencyTypes.count + index - 1)%pageCurrencyTypes.count

        switch pageViewController {
        case is FromCurrencyPageController:
            return self.viewControllerAtIndex(index, identifier: FromCurrencyController.storyboardID, storyboard: viewController.storyboard!)
        case is ToCurrencyPageController:
            return self.viewControllerAtIndex(index, identifier: ToCurrencyController.storyboardID, storyboard: viewController.storyboard!)
        default:
            return nil
        }
    }

    /// UIPageViewControllerDataSource.viewControllerAfterViewController handler
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = self.indexOfViewController(viewController as! CurrencyController)
        if index == NSNotFound {
            return nil
        }

        index = (index + 1)%pageCurrencyTypes.count
        switch pageViewController {
        case is FromCurrencyPageController:
            return self.viewControllerAtIndex(index, identifier: FromCurrencyController.storyboardID, storyboard: viewController.storyboard!)
        case is ToCurrencyPageController:
            return self.viewControllerAtIndex(index, identifier: ToCurrencyController.storyboardID, storyboard: viewController.storyboard!)
        default:
            return nil
        }
    }

    /// UIPageViewControllerDataSource.presentationCountForPageViewController handler
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }

    /// UIPageViewControllerDataSource.presentationIndexForPageViewController handler
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let startViewController = pageViewController.viewControllers?.first as? CurrencyController {
            return self.indexOfViewController(startViewController)
        }
        return 0
    }
}
