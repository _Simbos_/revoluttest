//
//  ExchangeModelManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import UIKit
import CoreData

/**
 **ExchangeModelManager** is a passive **Model** class. It doesn't control **View**. It's only a DataSource for **View** & **Controler** functions.
 **UIPageViewControllers** uses this class as DataSource.
 */
class ExchangeModelManager: NSObject {
    ///This handler is used for all currency operations which result should be rounded up.
    let roundUpHandler = NSDecimalNumberHandler(roundingMode: .up, scale: 0, raiseOnExactness: true, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true)
    ///This handler is used for all currency operations which result should be rounded down.
    let roundDownHandler = NSDecimalNumberHandler(roundingMode: .down, scale: 0, raiseOnExactness: true, raiseOnOverflow: true, raiseOnUnderflow: true, raiseOnDivideByZero: true)

    private var isCurrentFromValueOnDidSet = false
    /// Current amount of source currency.
    var currentFromValue: NSDecimalNumber?{
        didSet{
            if isCurrentToValueOnDidSet { return }

            isCurrentFromValueOnDidSet = true
            defer{ isCurrentFromValueOnDidSet = false }

            if let rate = currentRateFrom(currentFromCurrency, to: currentToCurrency){
                currentToValue = currentFromValue?.multiplying(by: rate, withBehavior: roundDownHandler)
            } else {
                currentToValue = nil
            }
        }
    }
    /// Current amount of destination currency.
    private var isCurrentToValueOnDidSet = false
    var currentToValue: NSDecimalNumber?{
        didSet{
            if isCurrentFromValueOnDidSet { return }
            if isCurrentRatePackOnDidSet { return }

            isCurrentToValueOnDidSet = true
            defer{ isCurrentToValueOnDidSet = false }

            if let rate = currentRateFrom(currentToCurrency, to: currentFromCurrency){
                currentFromValue = currentToValue?.multiplying(by: rate, withBehavior: roundUpHandler)
            } else {
                currentFromValue = nil
            }
        }
    }
    /// Current source currency type(USD,EUR,GBP...)
    var currentFromCurrency: CurrencyType = .USD
    /// Current destination currency type(USD,EUR,GBP...)
    var currentToCurrency: CurrencyType = .EUR

    private var isCurrentRatePackOnDidSet = false
    /// Current packet of currency rates which is used for exchange operations.
    var currentRatePack: CurrencyRatePack?{
        didSet{
            isCurrentRatePackOnDidSet = true
            defer { isCurrentRatePackOnDidSet = false }

            if let rate = currentRateFrom(currentFromCurrency, to: currentToCurrency){
                currentToValue = currentFromValue?.multiplying(by: rate, withBehavior: roundDownHandler)
            } else {
                currentToValue = nil
            }
        }
    }

    /**
     Returns current rate between two currencies.(**X** = (rate)x**Y**).
     - parameter fromCurrency: source currency type.
     - parameter toCurrency: destination currency type.
     - returns: rate's value or **nil** if it wasn't found.
     */
    func currentRateFrom(_ fromCurrency: CurrencyType, to toCurrency: CurrencyType) -> NSDecimalNumber? {
        guard currentRatePack != nil else {
            return nil
        }
        return CurrencyRatePack.rateOfPack(currentRatePack!, inContext: managedObjectContext, from: fromCurrency, to: toCurrency)
    }
    /// Observed managed object context.
    let managedObjectContext: NSManagedObjectContext

    /**
     Array of currency types related in order for UIPageViewControllers.
     */
    let pageCurrencyTypes: [CurrencyType]

    /// delegate for created pages
    weak var currencyViewControllerDelegate: CurrencyControllerDelegate?

    /**
     - parameter pageCurrencyTypes: array of currency types related in order for UIPageViewControllers.
     - parameter context: managed object context to observe.
     */
    init(pageCurrencyTypes: [CurrencyType], context: NSManagedObjectContext) {
        self.pageCurrencyTypes    = pageCurrencyTypes
        self.managedObjectContext = context
    }

    /**
     Returns an CurrencyController to present in UIPageViewController.
     - parameter index: index of CurrencyController in UIPageViewController.
     - parameter identifier: storyboard identifier of CurrencyController.
     - parameter storyboard: storyboard to search in.
     - returns: a new created CurrencyController or **nil** in case of some error.
     */
    func viewControllerAtIndex(_ index: Int, identifier: String, storyboard: UIStoryboard) -> CurrencyController? {
        // Return the data view controller for the given index.
        if (self.pageCurrencyTypes.count == 0) || (index >= self.pageCurrencyTypes.count) {
            return nil
        }

        // Create a new view controller and pass suitable data.
        let currencyViewController = storyboard.instantiateViewController(withIdentifier: identifier) as! CurrencyController
        currencyViewController.storedMoney = UserData.userData(inContext: managedObjectContext).moneyForCurrency(pageCurrencyTypes[index])
        currencyViewController.delegate    = currencyViewControllerDelegate

        switch currencyViewController {
        case is FromCurrencyController:
            currencyViewController.currencyType = pageCurrencyTypes[index]
            let rate = currentRateFrom(currentToCurrency, to: pageCurrencyTypes[index])
            if (currentToValue != nil) && (rate != nil){
                currencyViewController.value = currentToValue!.multiplying(by: rate!, withBehavior: roundUpHandler)
            } else {
                currencyViewController.value = nil
            }

        case is ToCurrencyController:
            (currencyViewController as? ToCurrencyController)?.toCurrencyType = pageCurrencyTypes[index]
            (currencyViewController as? ToCurrencyController)?.fromCurrencyType = self.currentFromCurrency
            let rate = currentRateFrom(currentFromCurrency, to: pageCurrencyTypes[index])
            (currencyViewController as? ToCurrencyController)?.exchangeRate = rate

            if (currentFromValue != nil) && (rate != nil){
                currencyViewController.value = currentFromValue!.multiplying(by: rate!, withBehavior: roundDownHandler)
            } else {
                currencyViewController.value = nil
            }

        default:
            ()
        }
        return currencyViewController
    }

    /**
     Returns index of CurrencyController in UIPageViewController.
     - parameter currencyViewController: CurrencyController from UIPageViewController.
     - returns: index of controller in UIPageViewController.
     */
    func indexOfViewController(_ currencyViewController: CurrencyController) -> Int {
        return pageCurrencyTypes.index(of: currencyViewController.currencyType) ?? NSNotFound
    }
    
}
