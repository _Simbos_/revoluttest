//
//  UserData.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/// default amount of just insereted **UserData**-object
private let moneyAmount = 10000

/**
 **UserData** is a subclass of **NSManagedObject**. It keeps information about user's amount of money. THere could be only one **UserData**-object in managed object context.
 */
class UserData: NSManagedObject {

    @NSManaged private var primitiveMoneyUSD: NSNumber
    @NSManaged private var primitiveMoneyEUR: NSNumber
    @NSManaged private var primitiveMoneyGBP: NSNumber
    /// Amount of USD in cents.
    var moneyUSD: NSDecimalNumber{
        get {
            willAccessValue(forKey:"moneyUSD")
            defer {
                didAccessValue(forKey:"moneyUSD")
            }
            return NSDecimalNumber(decimal: primitiveMoneyUSD.decimalValue)
        }

        set {
            willChangeValue(forKey:"moneyUSD")
            defer {
                didChangeValue(forKey:"moneyUSD")
            }
            primitiveMoneyUSD = newValue
        }
    }
    /// Amount of EUR in cents.
    var moneyEUR: NSDecimalNumber{
        get {
            willAccessValue(forKey:"moneyEUR")
            defer {
                didAccessValue(forKey:"moneyEUR")
            }
            return NSDecimalNumber(decimal: primitiveMoneyEUR.decimalValue)
        }

        set {
            willChangeValue(forKey:"moneyEUR")
            defer {
                didChangeValue(forKey:"moneyEUR")
            }
            primitiveMoneyEUR = newValue
        }
    }
    /// Amount of GBP in pences.
    var moneyGBP: NSDecimalNumber{
        get {
            willAccessValue(forKey:"moneyGBP")
            defer {
                didAccessValue(forKey:"moneyGBP")
            }
            return NSDecimalNumber(decimal: primitiveMoneyGBP.decimalValue)
        }

        set {
            willChangeValue(forKey:"moneyGBP")
            defer {
                didChangeValue(forKey:"moneyGBP")
            }
            primitiveMoneyGBP = newValue
        }
    }

    override func awakeFromInsert() {
        super.awakeFromInsert()
        uuid = NSUUID().uuidString.lowercased()
        moneyUSD = NSDecimalNumber(value: moneyAmount)
        moneyEUR = NSDecimalNumber(value: moneyAmount)
        moneyGBP = NSDecimalNumber(value: moneyAmount)
    }

    /**
     Gets amount of user's money specified by currency.
     - parameter currencyType: type of currency to search for.
     - returns: amount of specified money.
     */
    func moneyForCurrency(_ currencyType: CurrencyType) -> NSDecimalNumber {
        switch currencyType {
        case .USD:
            return moneyUSD
        case .EUR:
            return moneyEUR
        case .GBP:
            return moneyGBP
        }
    }

    /**
     Makes an **non-safety** exchange-operation of user's money.
     - parameter fromCurrency: currencyType to sell.
     - parameter fromValue: amount of money to cell.
     - parameter toCurrency: currencyType to buy.
     - parameter toValue: amount of money to buy.
     */
    func exchange(fromCurrency: CurrencyType, fromValue: NSDecimalNumber, toCurrency: CurrencyType, toValue: NSDecimalNumber){
        switch fromCurrency {
        case .USD:
            moneyUSD = moneyUSD.subtracting(fromValue)
        case .EUR:
            moneyEUR = moneyEUR.subtracting(fromValue)
        case .GBP:
            moneyGBP = moneyGBP.subtracting(fromValue)
        }

        switch toCurrency {
        case .USD:
            moneyUSD = moneyUSD.adding(toValue)
        case .EUR:
            moneyEUR = moneyEUR.adding(toValue)
        case .GBP:
            moneyGBP = moneyGBP.adding(toValue)
        }
    }

    /**
     Returns **UserData**-object from managed object context. This function inserts **UserData**-object in context if it doesn't exist.
     - parameter context: managed object context to search in.
     - returns: stored **UserData**-object.
     */
    class func userData(inContext context: NSManagedObjectContext) -> UserData {
        let fetchRequest: NSFetchRequest<UserData> = UserData.fetchRequest()
        fetchRequest.fetchBatchSize = 10

        if let userDataRecords = try? context.fetch(fetchRequest) {
            if let userData = userDataRecords.first {
                return userData
            }
        }

        let userData = UserData(entity: UserData.entity(), insertInto: context)
        return userData
    }
    
}
