//
//  UserData+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

extension UserData {
    @nonobjc class func fetchRequest() -> NSFetchRequest<UserData> {
        return NSFetchRequest<UserData>(entityName: "UserData")
    }

    /// Unique identificator of UserData record.
    @NSManaged var uuid: String
    
}
