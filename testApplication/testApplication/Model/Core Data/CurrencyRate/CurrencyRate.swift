//
//  CurrencyRate.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/**
 **CurrencyRate** is a subclass of **NSManagedObject**. It keeps information about rate of some currency.
 */
class CurrencyRate: NSManagedObject {

    @NSManaged private var primitiveCurrencyType: NSNumber
    /**
     Type of currency(USD, EUR, GBP...)
     */
    var currencyType: CurrencyType {
        get {
            willAccessValue(forKey:"currencyType")
            defer {
                didAccessValue(forKey:"currencyType")
            }

            guard let currencyType = CurrencyType(rawValue: primitiveCurrencyType.intValue) else {
                fatalError("Invalid primitiveCurrencyType(\(primitiveCurrencyType.intValue))" )
            }

            return currencyType
        }

        set {
            willChangeValue(forKey:"currencyType")
            defer {
                didChangeValue(forKey:"currencyType")
            }

            primitiveCurrencyType = NSNumber(value: newValue.rawValue)
        }
    }

    @NSManaged private var primitiveRate: NSNumber
    /**
     Current rate of currency. 1€ = (rate) x (amout of money in the currency)
     */
    var rate: NSDecimalNumber{
        get {
            willAccessValue(forKey:"rate")
            defer {
                didAccessValue(forKey:"rate")
            }
            return NSDecimalNumber(decimal: primitiveRate.decimalValue)
        }

        set {
            willChangeValue(forKey:"rate")
            defer {
                didChangeValue(forKey:"rate")
            }
            primitiveRate = newValue
        }
    }

    /**
     Inserts new rate to managed object context.
     - parameter pack: related pack of currency rate.
     - parameter context: managed object context where you want to insert new rate.
     - returns: inserted rate-object.
     */
    class func insertRateToPack(_ pack: CurrencyRatePack, inContext context: NSManagedObjectContext) -> CurrencyRate {
        let rate = CurrencyRate(entity: CurrencyRate.entity(), insertInto: context)

        let packID = pack.objectID
        guard let packInContext = context.object(with: packID) as? CurrencyRatePack else {
            fatalError("pack(\(pack)) not found in context(\(context))")
        }

        rate.pack = packInContext
        packInContext.addRate(rate)

        return rate
    }
}
