//
//  CurrencyRate+CoreDataProperties.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

extension CurrencyRate {
    @nonobjc class func fetchRequest() -> NSFetchRequest<CurrencyRate> {
        return NSFetchRequest<CurrencyRate>(entityName: "CurrencyRate")
    }

    /**
     Related **CurrencyRatePack** all **CurrencyRate**-objects are grouped in packs.
     */
    @NSManaged var pack: CurrencyRatePack
}

