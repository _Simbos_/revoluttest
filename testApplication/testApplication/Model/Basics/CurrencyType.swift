//
//  CurrencyType.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 Represents some currency type.
 * **USD**
 * **EUR**
 * **GBP**
 */
enum CurrencyType: Int, CustomStringConvertible, CustomDebugStringConvertible {
    /// United States dollar
    case USD = 0
    /// Euro
    case EUR
    /// Great Britain Pound
    case GBP

    /// array of possible values
    static let availableValues: [CurrencyType] = [.USD,.EUR,.GBP]

    /// abbreviation(ISO 4217 code) of currency.
    var abbreviation: String {
        switch self {
        case .USD: return "USD"
        case .EUR: return "EUR"
        case .GBP: return "GBP"
        }
    }

    /// symbol of currency.
    var symbol: String {
        switch self {
        case .USD:
            return "$"
        case .EUR:
            return "€"
        case .GBP:
            return "£"
        }
    }

    /**
     - parameter abbreviation: abbreviation(ISO 4217 code) of currency
     */
    init?(abbreviation: String){
        if let currencyIndex = CurrencyType.availableValues.index(where: { predicate in
            return predicate.abbreviation == abbreviation
        }){
            self = CurrencyType.availableValues[currencyIndex]
        } else {
            return nil
        }
    }

    /// Description string.
    var description: String {
        return abbreviation
    }

    /// Debug description string.
    var debugDescription: String {
        return abbreviation
    }

    /// formatter for integer-values(cent, euro-cent, pence) which is used to present amount of money in currency.
    var formatter: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle           = .currency
        formatter.multiplier            = NSNumber(value: 0.01)
        formatter.currencyCode          = self.abbreviation
        formatter.currencySymbol        = self.symbol
        formatter.maximumFractionDigits = 2

        return formatter
    }
}
