//
//  CurrencyRatePackParser.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

private let senderElementName = "Sender"
private let cubeElementName   = "Cube"
private let nameElementName   = "name"

private let serverTimeAttribute     = "time"
private let currencyTypeAttribute   = "currency"
private let currencyRateAttribute   = "rate"

/**
 This object parse information about currency rates from XML.
 */
class CurrencyRatePackParser: NSObject, XMLParserDelegate {

    private let xmlParser: XMLParser
    /// formatter for dates in XML
    private lazy var senderDateFormatter: DateFormatter = {
        let formatter        = DateFormatter()
        formatter.timeZone   = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    /// formatter for rates in XML
    private lazy var senderRateFromatter: NumberFormatter = {
        let formatter        = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = "."
        formatter.generatesDecimalNumbers = true
        return formatter
    }()

    /**
     - parameter fileURL: URL of XML-file.
     */
    init?(contentsOfURL fileURL: NSURL){
        guard let _xmlParser = XMLParser(contentsOf: fileURL as URL) else {
            return nil
        }
        self.xmlParser = _xmlParser
    }

    /**
     - parameter data: data of XML.
     */
    init(data: Data){
        self.xmlParser = XMLParser(data: data)
    }

    /**
     **true** if parser is busy at the moment, otherwise **false**
     */
    var isParsing: Bool {
        return comletionBlock != nil
    }

    private var comletionBlock: ((_ errorMessage: String?,_ currencyRates: [CurrencyType: NSDecimalNumber],_ senderName: String?,_ senderDate: Date?) -> Void)?

    private var senderName: String?{
        willSet(newSenderName){
            if isParsing && (senderName != nil) && (newSenderName != nil) {
                debugLog("Warning! Bad format - sender name was rewrited durnig parsing.")
            }
        }
    }

    private var senderDate: Date? {
        willSet(newSenderDate){
            if isParsing && (senderDate != nil) && (newSenderDate != nil) {
                debugLog("Warning! Bad format - server date was rewrited durnig parsing.")
            }
        }
    }

    private var currencyRates = [CurrencyType: NSDecimalNumber]()

    private var errorMessage: String?

    /**
     Starts parse-operation.
     - parameter comletion: completion block.
     */
    func parse(comletion: @escaping (_ errorMessage: String?,_ currencyRates: [CurrencyType: NSDecimalNumber],_ senderName: String?,_ senderDate: Date?) -> Void){
        guard !isParsing else {
            comletion("CurrencyRatePackParser is already parsing",
                      [CurrencyType: NSDecimalNumber](),
                      nil,
                      nil)
            return
        }

        comletionBlock = comletion

        xmlParser.shouldProcessNamespaces = true
        xmlParser.shouldReportNamespacePrefixes = true
        xmlParser.delegate = self

        senderName = nil
        senderDate = nil
        currencyRates.removeAll()
        errorMessage = nil

        senderEntered = false
        nameEntered   = false

        xmlParser.parse()
    }

    // MARK: - NSXMLParserDelegate
    private var senderEntered = false
    private var nameEntered   = false

    // MARK: - NSXMLParserDelegate

    /// NSXMLParserDelegate.parserDidEndDocument handler.
    func parserDidEndDocument(_ parser: XMLParser) {
        comletionBlock?(errorMessage, currencyRates, senderName, senderDate)
    }

    /// NSXMLParserDelegate.parser(:didStartElement:namespaceURI:qualifiedName:attributes:) handler.
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        switch elementName {
        case senderElementName:
            senderEntered = true
        case nameElementName:
            nameEntered = true
        case cubeElementName:
            if let serverTimeString = attributeDict[serverTimeAttribute] {
                if let senderDate = self.senderDateFormatter.date(from: serverTimeString) {
                    self.senderDate = senderDate
                } else{
                    debugLog("'\(serverTimeAttribute)' == \(serverTimeString). Bad format.")
                }
            }

            if let currencyTypeString = attributeDict[currencyTypeAttribute] {
                guard let currencyType = CurrencyType(abbreviation: currencyTypeString) else {
                    return
                }

                guard let currencyRateString = attributeDict[currencyRateAttribute] else {
                    debugLog("Currency '\(currencyType.abbreviation)' don't have rate")
                    return
                }

                guard let currencyRate = self.senderRateFromatter.number(from: currencyRateString) as? NSDecimalNumber else {
                    debugLog("Currency '\(currencyType.abbreviation)'. Rate is not a number '\(currencyRateString)'.")
                    return
                }

                if currencyRates[currencyType] != nil {
                    errorMessage = "Bad format! Currency '\(currencyType.abbreviation)' - rate was rewite durnig parsing"
                    parser.abortParsing()
                } else {
                    currencyRates[currencyType] = currencyRate
                }
            }
        default:
            ()
        }
    }

    /// NSXMLParserDelegate.parser(:didEndElement:namespaceURI:qualifiedName:) handler.
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case senderElementName:
            senderEntered = false
        case nameElementName:
            nameEntered = false
        default:
            ()
        }
    }

    /// NSXMLParserDelegate.parser(:foundCharacters:) handler.
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if senderEntered && nameEntered {
            senderName = string
        }
    }

    /// NSXMLParserDelegate.parser(:parseErrorOccurred:) handler.
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        if errorMessage != nil {
            comletionBlock?(errorMessage!,
                            [CurrencyType: NSDecimalNumber](),
                            nil,
                            nil)
        } else {
            comletionBlock?("\(parseError)", [CurrencyType: NSDecimalNumber](), nil, nil)
        }
    }

    /// NSXMLParserDelegate.parser(:validationErrorOccurred:) handler.
    func parser(_ parser: XMLParser, validationErrorOccurred validationError: Error) {
        comletionBlock?("\(validationError)", [CurrencyType: NSDecimalNumber](), nil, nil)
    }
}
