//
//  CurrencyRateManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import CoreData

/// Update interval
private let currencyRatesUpdateInterval: TimeInterval = 30.0

/**
 This controller observes server's information about currency rates by periodical network requests. Each request-result is packed and pushed to managed object context as **CurrencyRatePack**.
 */
class CurrencyRateManager: NSObject {
    /// Managed object context for insert operations.
    let managedObjectContext: NSManagedObjectContext

    /**
     - parameter managedObjectContext: managed object context for insert operations.
     */
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
        super.init()
    }

    private var updateRateTimer: Timer?

    /**
     Starts observe of server-data.
     */
    func startCurrencyRateUpdating(){
        if updateRateTimer != nil { return }
        Timer.scheduledTimer(timeInterval: currencyRatesUpdateInterval, target: self, selector: #selector(CurrencyRateManager.updateCurrencyRates), userInfo: nil, repeats: true)
        updateRateTimer?.fire()
    }

    private var isUpdating = false
    /**
     Makes network-request & pushes results to managed object context.
     This methos is called by timer. Don't use it manually.
     */
    func updateCurrencyRates() {
        guard !isUpdating else {
            return
        }
        isUpdating = true
        NetworkManager.getCurrencyRatesXML { result in
            switch result {
            case .Success(let dataXML):
                let currencyParser = CurrencyRatePackParser(data: dataXML)
                currencyParser.parse(comletion: { (errorMessage, currencyRates, senderName, senderDate) in
                    guard errorMessage == nil else {
                        debugLog("Parse Error: \(errorMessage!)")
                        self.isUpdating = false
                        return
                    }

                    let updatesManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
                    updatesManagedObjectContext.parent = self.managedObjectContext

                    updatesManagedObjectContext.perform {
                        let newPack = CurrencyRatePack.insertRatePack(inContext: updatesManagedObjectContext)
                        newPack.serverDate = senderDate.map { NSDate(timeInterval: 0, since: $0) }
                        newPack.sender     = senderName
                        for (currencyType, currencyRate) in currencyRates {
                            let newRate = CurrencyRate.insertRateToPack(newPack, inContext: updatesManagedObjectContext)
                            newRate.currencyType = currencyType
                            newRate.rate         = currencyRate
                        }
                        do {
                            try updatesManagedObjectContext.save()
                        } catch {
                            debugLog("Error: \(error)")
                        }

                        self.managedObjectContext.perform {
                            do {
                                try self.managedObjectContext.save()
                                //self.managedObjectContext.reset()
                            } catch {
                                debugLog("Error: \(error)")
                            }
                            self.isUpdating = false
                        }
                    }
                })

            case .Failure(_, let error):
                debugLog("error: \(error)")
                self.isUpdating = false
            }
        }
    }
}
