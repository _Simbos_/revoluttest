//
//  NetworkManager.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation
import Alamofire

private let currencyRateXMLAddress = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"

/// Abstract result of some Request-Operation
enum RequestResult<Value, RequestError: Error> {
    /// Success result.
    case Success(Value)
    /// Failure result.
    case Failure(Value?, Error)

    /// Returns `true` if the result is a success, `false` otherwise.
    var isSuccess: Bool {
        switch self {
        case .Success:
            return true
        case .Failure:
            return false
        }
    }

    /// Returns `true` if the result is a failure, `false` otherwise.
    var isFailure: Bool {
        return !isSuccess
    }

    /// Returns the associated value if the result is a success, `nil` otherwise.
    var value: Value? {
        switch self {
        case .Success(let value):
            return value
        case .Failure:
            return nil
        }
    }

    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    var error: Error? {
        switch self {
        case .Success:
            return nil
        case .Failure(_,let error):
            return error
        }
    }
}

/**
 NetworkManager makes HTTP-request to external server.
 */
class NetworkManager {
    /// Error generated by NetworkManager.
    enum NetworkManagerError: Error {
        /// Some connection error.
        case NetworkError
        /// Some server's error.
        case ServerError
        /// Server didn't return any response.
        case HTTPResponseNotAvailable
        /// Server returned HTTP status 400. "Bad request".
        case HTTPBadRequest
        /// Server returned HTTP status 401. "Unauthorized".
        case HTTPUnauthorized
        /// Server returned HTTP status 403. "Forbidden".
        case HTTPForbidden
        /// Server returned HTTP status 404. "Not Found".
        case HTTPNotFound
        /// Server returned HTTP status 500. "Internal Server Error".
        case HTTPInternalServerError
        /// Server returned unrecognized HTTP status. "Unsuccess Code".
        case HTTPUnsuccessCode
        /// Server returned HTTP status 422. "Wrong Fields".
        case HTTPWrongFields
        /// Server returned HTTP status 405. "Not Allowed".
        case HTTPNotAllowed
        /// Server returned HTTP status 415. "Wrong Data Type".
        case HTTPWrongDataType
        /// Server returned HTTP status 429. "Too Many Requests".
        case HTTPTooManyRequests
        case UnknownError
    }

    /// result of getCurrencyRatesXML(:) - request
    typealias RateRequestResult = RequestResult<Data,NetworkManager.NetworkManagerError>

    /**
     Makes request for new data about currency rates.
     - parameter completion: completion block.
     */
    class func getCurrencyRatesXML(completion: ((RateRequestResult) -> Void)? = nil) {
        Alamofire.request(currencyRateXMLAddress, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { response in

            let requestResult: RateRequestResult

            if let dataXML = response.result.value{
                if response.result.isSuccess {
                    if let httpResponse = response.response {
                        switch httpResponse.statusCode {
                        case 200, 201, 204:
                            requestResult = RateRequestResult.Success(dataXML)
                        case 400:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPBadRequest)
                        case 401:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPUnauthorized)
                        case 403:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPForbidden)
                        case 404:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPNotFound)
                        case 405:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPNotAllowed)
                        case 415:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPWrongDataType)
                        case 422:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPWrongFields)
                        case 429:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPTooManyRequests)
                        case 500:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPInternalServerError)
                        default:
                            requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPUnsuccessCode)
                        }
                    } else {
                        requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.HTTPResponseNotAvailable)
                    }
                } else {
                    requestResult = RateRequestResult.Failure(dataXML, NetworkManager.NetworkManagerError.ServerError)
                }
            } else {
                requestResult = RateRequestResult.Failure(nil, NetworkManager.NetworkManagerError.NetworkError)
            }
            completion?(requestResult)
        }
    }

    /// result of postExchangeOperation(fromCurrency:fromValue:toCurrency:toValue:completion:)
    typealias PostExchangeOperationResult = RequestResult<Bool,NetworkManager.NetworkManagerError>

    /**
     Makes request for new exchange operation.
     - parameter fromCurrency: source currency type.
     - parameter fromValue: amount of source currency.
     - parameter toCurrency: destination currency.
     - parameter toValue: amount of destination currency.
     - parameter completion: completion block.
     - warning: this function just emulates the network request.
     */
    class func postExchangeOperation(fromCurrency: CurrencyType, fromValue: NSDecimalNumber, toCurrency: CurrencyType, toValue: NSDecimalNumber, completion: ((PostExchangeOperationResult) -> Void)? = nil) {
        DispatchQueue.global(qos: .default).async {
            completion?(PostExchangeOperationResult.Success(true))
        }
    }
}
