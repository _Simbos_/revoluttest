//
//  Debug.swift
//  testApplication
//
//  Created by Andrew Simvolokov on 05/01/2017.
//  Copyright © 2017 Andrew Simvolokov. All rights reserved.
//

import Foundation

/**
 This function is used for all debug ouputs. It outputs message, filename, function name and line where it was called.
 */
func debugLog(_ message: String,
              file: StaticString = #file,
              function: StaticString = #function,
              line: Int = #line){

    let output: String
    if let filename = file.description.components(separatedBy: "/").last {
        output = "\(filename) \(function) line \(line) : \(message)"
    } else {
        output = "\(file).\(function) line \(line) : \(message)"
    }
    NSLog("%@", output)
}
