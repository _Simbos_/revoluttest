//
//  testApplicationUITests.swift
//  testApplicationUITests
//
//  Created by Andrew Simvolokov on 26/12/2016.
//  Copyright © 2016 Andrew Simvolokov. All rights reserved.
//

import XCTest

/**
 Here are some examples of UI-Tests.
 */
class testApplicationUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        XCUIApplication().launch()
    }

    /**
     App is entering a very big value of money to sell.
     */
    func testBigMoneyToSell() {
        let app = XCUIApplication()

        app.textFields["SellTextField"].tap()

        app.keys["9"].tap()
        app.keys["9"].tap()
        app.keys["9"].tap()

        let exchangeButton = app.navigationBars["testApplication.Exchange"].buttons["Exchange"]
        if exchangeButton.isEnabled {
            XCTFail("ExchangeButton is enabled, while user doesn't have that amount of money")
        }
    }

    /**
     App enter 1 amount of omeny to sell. Then script checks that it was exchanged correctly.
     */
    func testExchangeRate(){
        let app = XCUIApplication()

        let sellTextField = app.textFields["SellTextField"]
        guard sellTextField.exists else {
            XCTFail("Sell TextField was not found in interface")
            return
        }
        sellTextField.tap()
        app.keys["1"].tap()

        let rateStaticText = app.descendants(matching: .staticText).element(matching: .staticText, identifier: "RateLabel")
        guard rateStaticText.exists else {
            XCTFail("Rate-label was not found in interface")
            return
        }

        guard rateStaticText.label != "exchange unavailable" else {
            return
        }

        let rateParts = rateStaticText.label.components(separatedBy: "=")
        guard rateParts.count == 2 else {
            XCTFail("unexpected rate-label:\(rateStaticText.label)")
            return
        }

        let rightPart = rateParts[1]
        var rateString = rightPart.substring(from: rightPart.index(rightPart.startIndex, offsetBy: 1))
        //remove zeros & spaces
        while rateString.characters.last  == "0" ||
            rateString.characters.last == " " {
                rateString = rateString.substring(to: rateString.index(before: rateString.endIndex))
        }

        let buyTextField = app.textFields["BuyTextField"]
        guard buyTextField.exists else {
            XCTFail("Buy TextField was not found in interface")
            return
        }


        if buyTextField.value as! String != "+" + rateString {
            XCTFail("Buy TextField contains:'\(buyTextField.value)', but it should contains '+\(rateString)'")
        }
    }

}
